### Apache LBS

The current Apache LBS setup uses sticky sessions to forward requests to the same backend server instance (i.e., `backend-service-X`, where `X` is a counter). Sticky session header is set on initial client request and passed to the client as a `Set-Cookie` header in the response. For the `backend-service` the client could be another microservice. Whenever the request (which should be all subsequent requests) has no `Cookie` corresponding to the one set in `Set-Cookie` header, the response from the Apache LBS will contain a new `Set-Cookie` header.

You could test the Apache Load Balancing Server by running it on your local machine on port 9000 and at the same time running more than one containers with the `backend-service`.

```curl
# initial request (response will contain `Set-Cookie` header):
curl -i localhost:9000/message

# You should receive a response similar to:
# HTTP/1.1 200 OK
# Date: Mon, 11 Sep 2023 23:54:04 GMT
# Server: Apache/2.4.57 (Unix)
# X-Powered-By: Express
# Content-Type: text/html; charset=utf-8
# Content-Length: 41
# ETag: W/"29-YmlGJ0Ah4dBU6L5+gbaFzXHGeXs"
# Set-Cookie: ROUTEID=.node2; path=/message;HttpOnly;Secure
#
# Hello World! My id is 0.27104308086944395%

# subsequent request (use the `Set-Cookie` header from initial response to create the `Cookie` header for your subsequent requests):
curl -i localhost:9000/message -H "Cookie: ROUTEID=.node2; path=/message;HttpOnly;Secure"

# You should receive a response similar to:
#  curl -i -H GET localhost:9000/message -H "Cookie: ROUTEID=.node2; path=/message;HttpOnly;Secure"
# HTTP/1.1 200 OK
# Date: Mon, 11 Sep 2023 23:54:59 GMT
# Server: Apache/2.4.57 (Unix)
# X-Powered-By: Express
# Content-Type: text/html; charset=utf-8
# Content-Length: 41
# ETag: W/"29-q0Y2Vk39tNK87vp1vFZmw3zev1Q"
# 
# Hello World! My id is 0.27104308086944395%

# When you check the logs of the respective service, you should se logs with a counter like:
# docker logs load-balancers-backend-service-2
# 
# > backend-service@1.0.0 start
# > node ./src/server.js
# 
# Example app listening on port 3000
# Mon Sep 11 2023 23:54:54 GMT+0000 (Coordinated Universal Time) Request number 1 was made!
# Mon Sep 11 2023 23:54:56 GMT+0000 (Coordinated Universal Time) Request number 2 was made!
# Mon Sep 11 2023 23:54:59 GMT+0000 (Coordinated Universal Time) Request number 3 was made!
```