const express = require('express')
const app = express()
const port = 3000
const serviceId = Math.random()
let counter = 1

app.get('/message', (req, res) => {
  res.send(`Hello World! My id is ${serviceId}`)
  console.log(`${Date()} Request number ${counter++} was made!`)
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})